# Trivia 

### Name
SQL Trivia 1

#### Description
The oldest SQL Injection Vulnerability. The flag is the vulnerability ID.

#### Flag
flag{CVE-2000-1233}

#### Hints
This Vulnerability was for Phorum 3.0.7

Did you look at CVE Details???



### Internal Note
https://www.cvedetails.com/cve/CVE-2000-1233/
