# Trivia 

### Name
SQL Trivia 2

#### Description
In MSSQL Injection Whats the query to see what version it is? 

#### Flag
flag{SELECT @@version}
flag{SELECT_@@version}

#### Hints
There are a lot of cheat sheets out there.
